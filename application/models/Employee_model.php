<?php 

class Employee_model extends CI_Model{

	public function add_employee($data){
		 $q=$this->db->insert('employee_details',$data);
		 if($this->db->affected_rows() > 0){
		 	  return true;
		 }else{
		 	  return false;
		 } 
	}

	public function check_email($email){
		  $this->db->where('email',$email);
		  $q=$this->db->get('employee_details');
		  if($q->num_rows() > 0){
		  	 return true;
		  }else{
		  	return false; 
		  }
	}

	public function get_employee($limit,$offset){
	 	     $this->db->order_by('emp_id','DESC');
		     $this->db->limit($limit,$offset);
	 	  $q=$this->db->get('employee_details');
	 	  	  $html='';

	 	  if($q->num_rows() > 0){
	 	  	  foreach($q->result() as $emp){
	 	  	  	  $html .='
                <tr>
                    <td>'.$emp->emp_id.'</td>
                    <td>'.$emp->firstname.'</td>
                    <td>'.$emp->lastname.'</td>
                    <td>'.$emp->email.'</td>
                    <td>'.$emp->address.'</td>
                    <td>'.$emp->mobile.'</td>
                    <td>
                       <button type="submit" class="btn btn-warning update" id="'.$emp->emp_id.'" data-toggle="modal" data-target="#edit-emp">Edit</button>
                        <button type="submit" class="btn btn-danger delete" id="'.$emp->emp_id.'">Delete</button>
                    </td>
                </tr>
	 	  	  	  ';
	 	  	  }
	 	  	  return $html;
	 	  }else{
	 	  	  return $html.='<tr>
             <td>No Employees</td>  
	 	  	  </tr>';
	 	  }
	 }

	 public function count_employee(){
	 	    $q=$this->db->get('employee_details');
	 	    if($q->num_rows() > 0){
	 	    	 return $q->num_rows();
	 	    }
	 	   }

	 	public function get_user_details($emp_id){
	 	  $this->db->where('emp_id',$emp_id);
	 	  $q=$this->db->get('employee_details');
	 	  if($q->num_rows() > 0){
	 	  	 return $q->result();
	 	  }
	 }

	 public function update_employee($id){
	 	  $data=[
        'firstname'=>$this->input->post('efname'), 
        'lastname'=>$this->input->post('elname'), 
        'email'=>$this->input->post('editemail'), 
        'mobile'=>$this->input->post('emobile'), 
        'address'=>$this->input->post('eaddress'), 
	 	  ];
	 	  $this->db->where('emp_id',$id);
	 	  $this->db->update('employee_details',$data);
	 	  if($this->db->affected_rows() > 0){
	 	  	return true;
	 	  }else{
	 	  	return false;
	 	  }
	 }


	 public function delete_employee($id){
	 	 	  $this->db->where('emp_id',$id);
	 	 	  $q=$this->db->delete('employee_details');
	 	 	  if($this->db->affected_rows() > 0){
	 	 	  	 return true;
	 	 	  }else{
	 	 	  	return false;
	 	 	  }
	 	 }

   

}
?>